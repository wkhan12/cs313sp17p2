


1.	// TODO also try with a LinkedList - does it make any difference?
        // LinkedList will need to add pointers after each insertion.
2.	// TODO fix the expected values in the assertions below
	assertTrue(i.hasNext());
	assertEquals(77, i.next().intValue());
	assertTrue(i.hasNext());
	assertEquals(44, i.next().intValue());
	assertTrue(i.hasNext());
	assertEquals(77, i.next().intValue());
	assertTrue(i.hasNext());
	assertEquals(55, i.next().intValue());
	assertTrue(i.hasNext());
	assertEquals(77, i.next().intValue());
	assertTrue(i.hasNext());
	assertEquals(66, i.next().intValue());
	assertFalse(i.hasNext());
3.	// TODO what happens if you use list.remove(77)?
        // There are multiple 77 in the array and will return an error.
4.	// TODO using assertEquals and Arrays.asList, express which values are left in the list
	// See TestList.java for examples of how to use Arrays.asList; also see the Java Arrays
	// class for more information
	//fail("Not yet implemented"); // remove this line when done
        assertEquals(Arrays.asList(33,44,55,66),list);
5.	// TODO use an iterator and a while loop to compute the average (mean) of the values
	// (defined as the sum of the items divided by the number of items)
	// testNonempty shows how to use an iterator; use i.hasNext() in the while loop condition
	final Iterator<Integer> i = list.iterator();
        while (i.hasNext()) {
            sum = sum + i.next().intValue();
            n++;
        }
6.	// TODO also try with a LinkedList - does it make any difference?
	list = new LinkedList<Integer>(); // linked list also works fine
	
7.	// TODO fix the expected values in the assertions below
	list.add(77);
	assertEquals(false, list.isEmpty());
	assertEquals(1, list.size());
	assertEquals(77, list.get(0).intValue());	
8.	// TODO write assertions using
	// list.contains(77)
	// that hold before and after adding 77 to the list
	//fail("Not yet implemented"); // remove this line when done
	//assertEquals(77,list.get(0).intValue());
	assertEquals(false,list.contains(77));
	list.add(77);
	assertEquals(true,list.contains(77));
9.		// TODO fix the expected values in the assertions below
		assertEquals(3, list.size());
		assertEquals(0, list.indexOf(77));
		assertEquals(77, list.get(1).intValue());
		assertEquals(2, list.lastIndexOf(77));
10.		// TODO fix the expected values in the assertions below
		assertEquals(7, list.size());
		assertEquals(1, list.indexOf(77));
		assertEquals(5, list.lastIndexOf(77));
		assertEquals(44, list.get(2).intValue());
		assertEquals(77, list.get(3).intValue());
		assertEquals(Arrays.asList(33, 77, 44,77,55,77,66), list);
11.		// TODO fix the expected values in the assertions below
		assertEquals(6, list.size());
		assertEquals(1, list.indexOf(77));
		assertEquals(3, list.lastIndexOf(77));
		assertEquals(4, list.get(2).intValue());
		assertEquals(77, list.get(3).intValue());
		list.remove(Integer.valueOf(5)); // what does this one do?
		assertEquals(5, list.size());
		assertEquals(1, list.indexOf(77));
		assertEquals(3, list.lastIndexOf(77));
		assertEquals(4, list.get(2).intValue());
		assertEquals(77, list.get(3).intValue());
12.		// TODO using containsAll and Arrays.asList (see above),
		// 1) assert that list contains all five different numbers added
		// 2) assert that list does not contain all of 11, 22, and 33
		//fail("Not yet implemented"); // remove this line when done
		assertEquals(true,list.containsAll(Arrays.asList(33,44,55,77,66)));
		assertEquals(false,list.containsAll(Arrays.asList(11,22,33)));
13.		// TODO in a single statement using addAll and Arrays.asList,
		// add items to the list to make the following assertions pass
		// (without touching the assertions themselves)
		list.addAll(Arrays.asList(33,77,44,77,55,77,66));
		assertEquals(7, list.size());
		assertEquals(33, list.get(0).intValue());
		assertEquals(77, list.get(1).intValue());
		assertEquals(44, list.get(2).intValue());
		assertEquals(77, list.get(3).intValue());
		assertEquals(55, list.get(4).intValue());
		assertEquals(77, list.get(5).intValue());
		assertEquals(66, list.get(6).intValue());
14.		// TODO in a single statement using removeAll and Arrays.asList,
		// remove items from the list to make the following assertions pass
		// (without touching the assertions themselves)
		list.removeAll(Arrays.asList(33,44,55,66));
		assertEquals(3, list.size());
		assertEquals(Arrays.asList(77, 77, 77), list);
15.		// TODO in a single statement using retainAll and Arrays.asList,
		// remove items from the list to make the following assertions pass
		// (without touching the assertions themselves)
		list.retainAll(Arrays.asList(77,77,77));
		assertEquals(3, list.size());
		assertEquals(Arrays.asList(77, 77, 77), list);
16.		// TODO use the set method to change specific elements in the list
		// such that the following assertions pass
		// (without touching the assertions themselves)
		list.set(1,99);
		list.set(3,99);
		list.set(5,99);
17.		// TODO fix the arguments in the subList method so that the assertion
		// passes
		assertEquals(Arrays.asList(44, 77, 55), list.subList(2, 5));
18.// TODO run test and record running times for SIZE = 10, 100, 1000, 10000
		10:    AAR 24   LAR 22 AAc 12 LAc 12
		100:   AAR 37   LAR 26 AAc 14 LAc 27
		1000:  AAR 161  LAR 19 AAc 12 LAc 286
		10000: AAR 1437 LAR 20 AAc 13 LAc 3592
	Arraylist is faster on access while linkedlist is faster on adding and removing items.
